#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2\objdetect\objdetect.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <iostream>
#include <fstream>
#include <time.h>

using namespace cv;
using namespace std;

class Person{
	int frame;
	Rect rect;
	double score;
public:
	Person(){};
	Person(int, Rect, double){};
	int getFrame() {return frame;}
	Rect getSquare() {return rect;}
	double getScore() {return score;}
};

 CascadeClassifier person_cascade;

int main()
{
    clock_t t1, t2;
	t1 = clock();
	Mat image;
	std::vector<Rect> people;
	vector<int> rejectLevels;
	Person curPerson;
	vector<double> rejectWeights;
	vector<Person> allPeople;
	std::vector<String> dirs;
	String baseDIR = "D:/Chris/Documents/MATLAB/MS_project/";
	if( !person_cascade.load( "D:/opencv-src/sources/data/hogcascades/hogcascade_pedestrians.xml" ) ){ printf("--(!)Error loading\n"); return -1; };
	//image = imread("D:/Chris/Documents/MATLAB/MS_project/caltech/set08/V001/I00005.jpg");
	std::ifstream infile(baseDIR+"fileListC.txt");
	String dir;
	while(infile >> dir){
		people.clear();
		cout << baseDIR+dir << endl;
		cv::VideoCapture sequence;
		sequence.open(baseDIR+dir+"I%5d.jpg");
		ofstream fout(baseDIR+"toolbox/data-USA/res/HOG-Adaboost-10 neighbors/"+dir.substr(9, 5)+"/"+dir.substr(15,4)+".txt");
		int counter = 1;
		for(;;){
			sequence >> image;
			if (counter%30 ==0){
				if(! image.data )                              // Check for invalid input
				{
					cout <<  "Could not open or find the image" << std::endl ;
					break;
				}
				person_cascade.detectMultiScale( image, people, rejectLevels, rejectWeights, 1.05, 10, 0, Size(30, 30), Size(200, 200), true);
				for( size_t i = 0; i < people.size(); i++ )
				{
					
					fout << counter <<"," << (people[i].x) << "," << (people[i].y) << "," << (people[i].width) << "," << (people[i].height) << "," << rejectWeights[i] << endl;
				}
				//imshow( "Display window", image );                   // Show our image inside it.
				//waitKey(5);
			}
			counter++;
		}
		fout.close();
	}
  //-- Show what you got
	t2=clock();
    float diff ((float)t2-(float)t1);
    cout<< diff / CLOCKS_PER_SEC<<endl;
    system ("pause");
    return 0;
}