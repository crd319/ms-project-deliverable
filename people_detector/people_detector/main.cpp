#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/ml/ml.hpp"
#include <opencv2\gpu\gpu.hpp>
#include "LinearSVM.h"

#include <stdio.h>
#include <time.h>
#include <conio.h>
#include <string.h>
#include <ctype.h>
#include <iostream>
#include <fstream>
#include <direct.h>
#include "dirent.h"
#include <Windows.h>
#include <cmath>

using namespace cv;
using namespace std;

typedef struct people{
	int x,y,w,h;
	double score;

	bool operator < (const people& str) const{
		return ( score < str.score);
	}
};

// static void help()
// {
//     printf(
//             "\nDemonstrate the use of the HoG descriptor using\n"
//             "  HOGDescriptor::hog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());\n"
//             "Usage:\n"
//             "./peopledetect (<image_filename> | <image_list>.txt)\n\n");
// }


void trainSVM(gpu::HOGDescriptor &hog, LinearSVM &svm){

	cout << "training" << endl;

	CvSVMParams params;
	params.svm_type = CvSVM::C_SVC;
	params.kernel_type = CvSVM::LINEAR;
	params.C = 0.01;
    params.p = 0.5;

	HANDLE hFind;
	WIN32_FIND_DATA FindFileData;
	vector<float> supportVector;

	DIR *dir;

	Mat image;
	vector<vector<float>> trainData;
	vector<int> labels;
	int numPos =0;
	int numNeg = 0;
	String posDIR = "../../INRIAPerson/train_64x128_H96/pos/";
	dir = opendir(posDIR.c_str());

	string imgName;
	struct dirent *ent;
	std::cout << "Positive training" <<endl;
	if (dir != NULL){
		while ((ent = readdir(dir)) != NULL){
			vector<float> featureVector;
			imgName = ent->d_name;
			string imgPath(posDIR+ent->d_name);
			//cout << imgPath <<endl;
			Mat image = imread(imgPath);
			if( image.empty() )                              // Check for invalid input
			{
				std::cout <<  "Could not open or find the image" << std::endl ;
			}else{
				cv::cvtColor(image, image, CV_BGR2GRAY);
				//Mat resized_img = image(cv::Range(10,138),cv::Range(42,74));
				Mat resized_img = image(cv::Range(10,138),cv::Range(10,42));
				//Mat resized_img = image(cv::Range(10,74),cv::Range(10,74));
				gpu::GpuMat gpuImg(resized_img);
				gpu::GpuMat gpu_feature;
				hog.getDescriptors(gpuImg, Size(8,8), gpu_feature);
				Mat cpu_feature;
				gpu_feature.download(cpu_feature);
				const float* p = cpu_feature.ptr<float>(0);
				vector<float> featureVector(p, p + cpu_feature.cols);
				trainData.push_back(featureVector);
				labels.push_back(1);
				numPos ++;
				resized_img.release();
			}
		}
		closedir(dir);
	}
	posDIR = "../../INRIAPerson/test_64x128_H96/pos/";
	dir = opendir(posDIR.c_str());
	if (dir != NULL){
		while ((ent = readdir(dir)) != NULL){
			vector<float> featureVector;
			imgName = ent->d_name;
			string imgPath(posDIR+ent->d_name);
			//cout << imgPath <<endl;
			Mat image = imread(imgPath);
			if( image.empty() )                              // Check for invalid input
			{
				std::cout <<  "Could not open or find the image" << std::endl ;
			}else{
				cv::cvtColor(image, image, CV_BGR2GRAY);
				//Mat resized_img = image(cv::Range(5,133),cv::Range(37,69));
				Mat resized_img = image(cv::Range(5,133),cv::Range(5,37));
				//Mat resized_img = image(cv::Range(5,69),cv::Range(5,69));
				gpu::GpuMat gpuImg(resized_img);
				gpu::GpuMat gpu_feature;
				hog.getDescriptors(gpuImg, Size(8,8), gpu_feature);
				Mat cpu_feature;
				gpu_feature.download(cpu_feature);
				const float* p = cpu_feature.ptr<float>(0);
				vector<float> featureVector(p, p + cpu_feature.cols);
				trainData.push_back(featureVector);
				labels.push_back(1);
				numPos ++;
				resized_img.release();
			}
		}
		closedir(dir);
	}

	String negDIR = "../../INRIAPerson/Train/neg/";
	std::cout << "Negative training" <<endl;
	dir = opendir(negDIR.c_str());

	if (dir != NULL){
		while ((ent = readdir(dir)) != NULL){
			vector<float> featureVector;
			string imgPath(negDIR+ent->d_name);
			//cout << imgPath <<endl;
			Mat image = imread(imgPath);
			if(image.empty() )                              // Check for invalid input
			{
				std::cout <<  "Could not open or find the image" << std::endl ;
			}else{
				cv::cvtColor(image, image, CV_BGR2GRAY);
				for (int i=0; i<10;i++){
					int rx = rand() % (image.cols-32);
					int ry = rand() % (image.rows-128);
					//Mat resized_img = image(cv::Range(ry,ry+64), cv::Range(rx,rx+64));
					Mat resized_img = image(cv::Range(ry,ry+128), cv::Range(rx,rx+32));
					gpu::GpuMat gpuImg(resized_img);
					gpu::GpuMat gpu_feature;
					hog.getDescriptors(gpuImg, Size(8,8), gpu_feature);
					Mat cpu_feature;
					gpu_feature.download(cpu_feature);
					const float* p = cpu_feature.ptr<float>(0);
					vector<float> featureVector(p, p + cpu_feature.cols);
					trainData.push_back(featureVector);
					labels.push_back(-1);
					numNeg ++;
					resized_img.release();
				}
			}
		}
		closedir(dir);
	}
	
	cout << "Positive Samples: " << numPos << endl;
	cout << "Negative Samples: " << numNeg << endl;
	Mat trainMat(numPos+numNeg, trainData[0].size(), CV_32F);
	for (size_t i =0; i < numPos+numNeg; i++){
		for (size_t j =0; j < trainData[0].size(); j++){
			trainMat.at<float>(i,j) = trainData[i][j];
		}
	}
	Mat labelMat(labels);
	svm.train(trainMat, labelMat, Mat(), Mat(), params);
	svm.save("./model/leftProfile.xml");
	trainMat.release();
	labelMat.release();

	//Mat img = imread("../../INRIAPerson/Test/pos/crop_000005.png");
	//Mat img2 = imread("../../INRIAPerson/Test/pos/crop001501.png");
	//Mat img3 = imread("../../INRIAPerson/Test/pos/crop001723.png");

	vector<Mat> files;
	//files.push_back(img);
	//files.push_back(img2);
	//files.push_back(img3);
	
	svm.getSupportVector(supportVector);
	hog.setSVMDetector(supportVector);

	vector<Mat> tempFiles = files;

	cout <<"Deep Negative Training" << endl;

	bool negMine = true;
	vector<Rect> found, found_filtered;
	while(negMine){
		svm.getSupportVector(supportVector);
		hog.setSVMDetector(supportVector);
		dir = opendir(negDIR.c_str());
		int falseAlarms = 0;
		if (dir != NULL){
			while ((ent = readdir(dir)) != NULL){
				string imgPath(negDIR+ent->d_name);
				cout << imgPath <<endl;
				Mat image = imread(imgPath);
				found.clear(); found_filtered.clear();
				if(image.empty() )                              // Check for invalid input
				{
					std::cout <<  "Could not open or find the image" << std::endl ;
				}else{
					cv::cvtColor(image, image, CV_BGR2GRAY);
					gpu::GpuMat gpuImg(image);
					
					//vector<double> weight;
					hog.detectMultiScale(gpuImg, found, 0, Size(8,8), Size(0,0), 1.05, 2);

					size_t i, j;
					for( i = 0; i < found.size(); i++ )
					{
						Rect r = found[i];
						for( j = 0; j < found.size(); j++ )
							if( j != i && (r & found[j]) == r)
								break;
						if( j == found.size() )
							found_filtered.push_back(r);
					}
					for( i = 0; i < found_filtered.size(); i++ )
					{
						Rect r = found_filtered[i];
						vector<float> featureVector;
						r.x = cvRound(r.x);
						r.width = cvRound(r.width);
						r.y = cvRound(r.y);
						r.height = cvRound(r.height);
						//cout << r << endl;
						if(r.x+r.width <=image.cols && r.y+r.height <= image.rows){
							Mat subImg = image(r);
							resize(subImg, subImg, Size(32, 128));
							//resize(subImg, subImg, Size(64, 64));
							gpu::GpuMat gpuImg(subImg);
							gpu::GpuMat gpu_feature;
							hog.getDescriptors(gpuImg, Size(8,8), gpu_feature);
							Mat cpu_feature;
							gpu_feature.download(cpu_feature);
							const float* p = cpu_feature.ptr<float>(0);
							vector<float> featureVector(p, p + cpu_feature.cols);
							trainData.push_back(featureVector);
							labels.push_back(-1);
							numNeg++;
							falseAlarms++;
						}
					}
					found.clear();
					found_filtered.clear();
				}
			}
		}
		closedir(dir);
		cout << "Number of False Alarms: " << falseAlarms << endl;
		cout << "Positive Samples: " << numPos << endl;
		cout << "Negative Samples: " << numNeg << endl;
		cout << "Press Any Key To Stop Training";
		int waitTime = 200;
		int in = 1;
		while(1){
			if(kbhit()){
				char c=getch();
				in = 0;
				break;
			}
			Sleep(1000);
			--waitTime;

			if (waitTime==0){
				in = 1;
				break;
			}
		}

		if (in){
			Mat trainMat(numPos+numNeg, trainData[0].size(), CV_32F);
			cout << trainMat.rows << " " << trainMat.cols << endl;
			for (size_t i =0; i < numPos+numNeg; i++){
				for (size_t j =0; j < trainData[0].size(); j++){;
					trainMat.at<float>(i,j) = trainData[i][j];
				}
			}
			cout << trainMat.rows << " " << trainMat.cols << endl;
			Mat labelMat(labels);
			cout << labelMat.rows << " " << labelMat.cols << endl;
			svm.train(trainMat, labelMat, Mat(), Mat(), params);
			svm.save("./model/leftProfile.xml");
			trainMat.release();
			labelMat.release();
		}
		else{
			negMine = false;
		}
	}
}

void filterRect(vector<Rect> &found, vector<people> &found_filtered, vector<double> &weights, int type){
	size_t i, j;
        for( i = 0; i < found.size(); i++ )
        {
            Rect r = found[i];
			double w = weights[i];
			//double w = weights[i].confidences;
            //for( j = 0; j < found.size(); j++ )
            //    if( j != i && (r & found[j]) == r)
            //        break;
            //if( j == found.size() )
				switch(type){
					case 1:
						break;
					case 2:
						r.height = r.height*2;
						break;
					case 3:
						r.y = r.y-r.height;
						r.height = r.height*2; 
						break;
					case 4:
						r.x = r.x-r.width;
						r.width = r.width*2; 
						break;
					case 5:
						r.width = 2*r.width;
						break;
				}
				people found = {r.x,r.y,r.width,r.height,w};
				found_filtered.push_back(found);
        }
}

vector<people> non_max_suppression(vector<people> &detected, float thresh){
	vector<people> returnBoxes;
	if (detected.size() == 0){
		return returnBoxes;
	}

	int x0,y0,x1,y1,x00,y00,x11,y11;

	for (int i=0; i<detected.size();i++){
		returnBoxes.push_back(detected[i]);
		x0 = detected[i].x;
		y0 = detected[i].y;
		x1 = x0+detected[i].w;
		y1 = y0+detected[i].h;
		for (int j=i+1;j<detected.size();j++){
			x00 = detected[j].x;
			y00 = detected[j].y;
			x11 = x00+detected[j].w;
			y11 = y00+detected[j].h;

			int left = max(x0, x00);
			int right = min(x1,x11);
			int bottom = max(y0,y00);
			int top = min(y1, y11);

			int h = top-bottom;
			int w = right-left;

			float overlap1 = w*h/(float)(detected[i].h*detected[i].w);
			float overlap2 = w*h/(float)(detected[j].h*detected[j].w);

			//cout << overlap1 << " " << overlap2 << endl;

			if (overlap1 > thresh){
				detected.erase(detected.begin()+j);
			}
		}
	}
	return returnBoxes;
}

int main()
{
	clock_t t1,t2;
	t1=clock();
	cout << gpu::getCudaEnabledDeviceCount <<endl;
	gpu::setDevice(0);
	cout << gpu::getDevice <<endl;
	//cout << "inn " << endl;

    Mat img;
	Mat imgRz;
    FILE* f = 0;
    char _filename[1024];
	CvSVMParams params;
	params.svm_type = CvSVM::C_SVC;
	params.kernel_type = CvSVM::LINEAR;
	params.C = 0.01;

	LinearSVM sideSVM;

    // img = imread("../../INRIAPerson/Test/pos/crop_000005.png");
	// img = imread("../../INRIAPerson/Test/pos/crop001501.png");
	// img = imread("../../INRIAPerson/Test/pos/crop001723.png");

    if( img.data )
    {
        //strcpy(_filename, argv[1]);
    }
    else
    {
        //f = fopen(argv[1], "rt");
        //if(!f)
        //{
        //    fprintf( stderr, "ERROR: the specified file could not be loaded\n");
        //    return -1;
        //}
    }

	cout << "defining hog " << endl;
    //gpu::HOGDescriptor hog(Size(64,128), Size(16,16), Size(8,8), Size(8,8), 9);
	//HOGDescriptor hog(Size(64,128), Size(16,16), Size(8,8), Size(8,8), 9);
	HOGDescriptor hog;
	hog.winSize = Size(64,128);
	hog.blockSize = Size(16,16);
	hog.blockStride = Size(8,8);
	hog.cellSize = Size(8,8);
	hog.nbins = 9;
	//cout << "upper created " << endl;

	//Define Upper Body Hog Detector parameters
	
	//HOGDescriptor UpperBodyHog(Size(64,64), Size(16,16), Size(8,8), Size(8,8), 9);
	HOGDescriptor UpperBodyHog;
	UpperBodyHog.winSize = Size(64,64);
	UpperBodyHog.blockSize = Size(16,16);
	UpperBodyHog.blockStride = Size(8,8);
	UpperBodyHog.cellSize = Size(8,8);
	UpperBodyHog.nbins = 9;
	//Define Lower Body Hog Detector parameters
	//gpu::HOGDescriptor LowerBodyHog(Size(64,64), Size(16,16), Size(8,8), Size(8,8), 9);

	//gpu::HOGDescriptor RightBodyHog(Size(32,128), Size(16,16), Size(8,8), Size(8,8), 9);
	
	HOGDescriptor RightBodyHog;
	RightBodyHog.winSize = Size(32,128);
	RightBodyHog.blockSize = Size(16,16);
	RightBodyHog.blockStride = Size(8,8);
	RightBodyHog.cellSize = Size(8,8);
	RightBodyHog.nbins = 9;
	
	//gpu::HOGDescriptor LeftBodyHog(Size(32,128), Size(16,16), Size(8,8), Size(8,8), 9);
	HOGDescriptor LeftBodyHog;
	LeftBodyHog.winSize = Size(32,128);
	LeftBodyHog.blockSize = Size(16,16);
	LeftBodyHog.blockStride = Size(8,8);
	LeftBodyHog.cellSize = Size(8,8);
	LeftBodyHog.nbins = 9;

	vector<float> supportVectorUpper;
	vector<float> supportVectorLower;
	vector<float> supportVectorRight;
	vector<float> supportVectorLeft;
	LinearSVM svmUpper;
	LinearSVM svmLower;
	LinearSVM svmRight;
	LinearSVM svmLeft;

	std::ifstream upperSVM("./model/upperBody.xml");
	//std::ifstream lowerSVM("./model/lowerBody.xml");
	std::ifstream leftSVM("./model/leftProfile.xml");
	//std::ifstream rightSVM("./model/rightProfile.xml");

	/*
	if (!leftSVM){
		cout << "training" << endl;
		trainSVM(LeftBodyHog, svmLeft);
	}

	return 0;
	*/
	hog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());
	
	svmUpper.load("./model/upperBody.xml");
	svmUpper.getSupportVector(supportVectorUpper);
    UpperBodyHog.setSVMDetector(supportVectorUpper);
	/*
	svmLower.load("./model/lowerBody.xml");
	svmLower.getSupportVector(supportVectorLower);
    LowerBodyHog.setSVMDetector(supportVectorLower);
	*/
	svmRight.load("./model/rightProfile.xml");
	svmRight.getSupportVector(supportVectorRight);
    RightBodyHog.setSVMDetector(supportVectorRight);

	svmLeft.load("./model/leftProfile.xml");
	svmLeft.getSupportVector(supportVectorLeft);
    LeftBodyHog.setSVMDetector(supportVectorLeft);
	
    //namedWindow("people detector", 1);
	String baseDIR = "D:/Chris/Documents/MATLAB/MS_project/";
	//std::ifstream infile(baseDIR+"fileListC.txt");
	std::ifstream infile(baseDIR+"fileListTestC.txt");
	String dir;
	vector<Rect> foundFull, foundUpper, foundLower, foundLeft, foundRight, found_filtered;
	//vector<gpu::HOGConfidence> fullConf;
	vector<double> weightFull, weightUpper, weightLower, weightLeft, weightRight, weight_filtered;
	vector<people> foundPeople;
	while(infile >> dir){
		cout << baseDIR+dir << endl;
		cv::VideoCapture sequence;
		sequence.open(baseDIR+dir+"I%05d.jpg");
		//sequence.open(baseDIR+dir+"I00000.png");
		//namedWindow( "Display window", WINDOW_AUTOSIZE );// Create a window for display.
		ofstream fout(baseDIR+"toolbox/data-USA/res/demo/"+dir.substr(9, 5)+"/"+dir.substr(15,4)+".txt");
		//ofstream fout(baseDIR+"toolbox/data-INRIA/res/demo/set01/V000.txt");
		int counter = 1;
		for(;;){
			foundPeople.clear(); foundFull.clear(); foundUpper.clear(); foundLower.clear(); foundLeft.clear(); foundRight.clear(); found_filtered.clear();
			weightFull.clear(); weightUpper.clear(); weightLower.clear(); weightLeft.clear(); weightRight.clear(); weight_filtered.clear();
			//fullConf.clear();
			sequence >> img;
			
			if (counter%30 == 0){
			//if (true){
				if(! img.data )                              // Check for invalid input
				{
					cout <<  "Done" << std::endl ;
					break;
				}
				cout << counter << endl;
				cv::cvtColor(img, img, CV_BGR2GRAY);
				//resize(img, imgRz, Size(0,0), 1,1);
				resize(img, imgRz, Size(0,0), 3,3);
				//gpu::GpuMat gpuImg;
				//gpuImg.upload(img);
				//double t = (double)getTickCount();
				// run the detector with default parameters. to get a higher hit-rate
				// (and more false alarms, respectively), decrease the hitThreshold and
				// groupThreshold (set groupThreshold to 0 to turn off the grouping completely).
				hog.detectMultiScale(imgRz, foundFull, weightFull,0, Size(8,8), Size(8,8), 1.05, 2);
				//hog.computeConfidenceMultiScale(gpuImg, foundFull, 0, Size(8,8), Size(0,0), fullConf, 2);
				//hog.detectMultiScale(gpuimg, foundUpper, 0, Size(8,8), Size(0,0), 1.05, 2, );
				//UpperBodyHog.computeConfidenceMultiScale(gupImg, foundUpper, 0, Size(8,8), Size(8,8), upperConf, 2);
				UpperBodyHog.detectMultiScale(imgRz, foundUpper, weightUpper, 0, Size(8,8), Size(8,8), 1.05, 2);
				//LowerBodyHog.detectMultiScale(imgRz, foundLower, weightLower, 0, Size(8,8), Size(0,0), 1.05, 2);
				LeftBodyHog.detectMultiScale(imgRz, foundLeft, weightLeft, 0, Size(8,8), Size(0,0), 1.05, 2);
				RightBodyHog.detectMultiScale(imgRz, foundRight, weightRight, 0, Size(8,8), Size(0,0), 1.05, 2);

				//t = (double)getTickCount() - t;
				//printf("tdetection time = %gms\n", t*1000./cv::getTickFrequency());

				filterRect(foundFull, foundPeople, weightFull, 1);
				filterRect(foundUpper, foundPeople, weightUpper, 2);
				//filterRect(foundLower, foundPeople, weightLower, 3);
				filterRect(foundRight, foundPeople, weightRight, 4);
				filterRect(foundLeft, foundPeople, weightLeft, 5);

				sort(foundPeople.begin(), foundPeople.end());
				reverse(foundPeople.begin(), foundPeople.end());
				vector<people> suppressedDetected = foundPeople;
        
				//suppressedDetected = non_max_suppression(foundPeople, 0.3);
				
				for( int i = 0; i < suppressedDetected.size(); i++ )
				{
					//cout << suppressedDetected[i].score << endl;
					people r = suppressedDetected[i];

					fout << counter <<"," << (r.x)/3.0 << "," << (r.y)/3.0 << "," << (r.w)/3.0 << "," << (r.h)/3.0 << "," << r.score << endl;
					//fout << counter <<"," << (r.x)<< "," << (r.y) << "," << (r.w) << "," << (r.h) << "," << r.score << endl;
					//fout << counter <<"," << (r.x)<< "," << (r.y) << "," << (r.w) << "," << (r.h) << "," << r.score << endl;
					//rectangle(img, Point(r.x,r.y), Point(r.x+r.w, r.y+r.h), cv::Scalar(0,255,0), 3);
				}
			}
			counter++;
			//imshow("people detector", img);
			//int c = waitKey(0) & 255;
			img.empty();
			imgRz.empty();
		}
	}
		//imshow("people detector", img);
        //int c = waitKey(0) & 255;
	t2=clock();
	float diff ((float)t2-(float)t1);
    cout<<diff/CLOCKS_PER_SEC<<endl;
    system ("pause");
    return 0;
}
