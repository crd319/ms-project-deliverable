import os
import sys
import csv
import cv2
import glob
import numpy as np
import math
import timeit


if __name__ == "__main__":

    start = timeit.default_timer()
    with open('./fileList.txt') as f:
        files = f.readlines()
    files = [x.replace("\n", "") for x in files]

    hog = cv2.HOGDescriptor()
    hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
    
    
    for dirName in files:
        print dirName
        imgs = glob.glob(dirName)
        counter = 1
        targets = []
        spl = dirName.split('/')
        for fn in imgs:
            if (counter % 30 == 0):
                img = cv2.imread(fn);

                rz = cv2.resize(img, (0,0), fx = 3, fy= 3)
                bb, w = hog.detectMultiScale(rz, winStride=(8,8), padding=(32,32), scale=1.05)
                for i in range(len(w)):
                    targets.append((counter, bb[i][0]/3, bb[i][1]/3, bb[i][2]/3, bb[i][3]/3, w[i][0]))
                #for (x,y,w,h) in bb:
                #    cv2.rectangle(img,(int(x/3),int(y/3)),(int((x+w)/3),int((y+h)/3)),(255,0,0),2)
                #   roi_color = img[y:y+h, x:x+w]
            
                #cv2.imshow('Haar',img)
                #key = cv2.waitKey(0)
                #if key == 27:
                #    sys.exit(0)
            counter = counter+1
        with open('./toolbox/data-USA/res/HOG-opencv-resize4x4/'+spl[2]+'/'+spl[3]+'.txt', 'w') as f:
            writer = csv.writer(f)
            writer.writerows(targets)
    stop = timeit.default_timer()
    print stop - start 
    cv2.destroyAllWindows();
    print "complete"
