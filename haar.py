import os
import sys
import csv
import cv2
import glob
import numpy as np
import math
import timeit


if __name__ == "__main__":

    start = timeit.default_timer()
    with open('./fileList.txt') as f:
        files = f.readlines()
    files = [x.replace("\n", "") for x in files]
    for dirName in files:
        print dirName
        imgs = glob.glob(dirName)
        counter = 1
        targets = []
        spl = dirName.split('/')
        for fn in imgs:
            if (counter % 30 == 0):
                img = cv2.imread(fn);

                detector = cv2.CascadeClassifier("haarcascade_fullbody.xml")
                bb, r, weights = detector.detectMultiScale3(img, scaleFactor = 1.05, minNeighbors = 4,
                                               minSize = (10,10),flags = cv2.CASCADE_SCALE_IMAGE,
                                                outputRejectLevels=1)

                for i in range(len(weights):
                    targets.append((counter, bb[i][0], bb[i][1], bb[i][2], bb[i][3]))
                for (x,y,w,h) in bb:
                    cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
                   roi_color = img[y:y+h, x:x+w]
            
                #cv2.imshow('Haar',img)
                #key = cv2.waitKey(1)
                #if key == 27:
                #    sys.exit(0)
            counter = counter+1
        with open('./toolbox/data-USA/res/Haar 0 neighbors/'+spl[2]+'/'+spl[3]+'.txt', 'w') as f:
            writer = csv.writer(f)
            writer.writerows(targets)
    stop = timeit.default_timer()
    print stop - start 
    cv2.destroyAllWindows();
    print "complete"
